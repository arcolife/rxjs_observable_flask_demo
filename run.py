#!/env/bin python

import ast
import os, sys
import socket
import requests, json
# import subprocess
# from subprocess import call

from app import app

from flask import jsonify, \
    request, \
    Response, \
    render_template

# app = Flask(__name__, static_url_path='')


@app.route('/', methods=['GET'])
def home():
    return render_template('home.html')


@app.route('/status/', methods=['GET'])
def worker_status():
    return jsonify({'client_test': 'OK'})


# @app.route('/test/client/', methods=['POST'])
# def test():
#     print request.data
#     return jsonify({'got response from server': 'OK'})


# @app.route('/tasks/', methods=['POST'])
# def get_tasks():
#     BUSY = 1
#     # received = ast.literal_eval(request.data)
#     received = json.loads(request.data)
#     #print received[0]
#     result = 0
#     for i in received['dataset']:
#         result += sum(i)
#     BUSY = 0
#     return Response(str(result))


if __name__ == '__main__':
    try:
        app.run(
            host = 'localhost',
            port = 5000,
            debug = True
        )
    except:
        raise
