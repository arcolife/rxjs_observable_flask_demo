# Aync file processing and frontend loading with Python based web app

We're using: RxJS Observables + Python  Flask + Socket.IO demo

## Steps

1. Install virtualenv
2. create a python 3 venv/ folder
3. `source venv/bin/activate`
4. `pip install -r requirements.txt`
5. `python run.py`

Your application is now live at https://localhost:5000/

### helper links

- Simple usage: https://codepen.io/mmiszy/pen/jGwzzG
- RxJS lib: https://cdnjs.cloudflare.com/ajax/libs/rxjs/5.4.0/Rx.js

- https://hackernoon.com/understanding-creating-and-subscribing-to-observables-in-angular-426dbf0b04a3
- https://codecraft.tv/courses/angular/http/http-with-observables/

### server side (if using python)

- (uses websocket though) https://gearheart.io/blog/auto-websocket-reconnection-with-rxjs/

  - Code example ^ - https://github.com/TigorC/rxjs_websocket

- https://github.com/ReactiveX/RxPY

### server side (if using nodejs)

- https://www.npmjs.com/package/rxjs-socket.io

