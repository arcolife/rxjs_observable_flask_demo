import os
from flask import Flask, render_template, json, session

BASE_DIR = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__,  template_folder=os.path.join(BASE_DIR, 'templates'))
